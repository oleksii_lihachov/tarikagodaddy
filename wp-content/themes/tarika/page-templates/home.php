<?php
/*
Template Name: Homepage
*/

get_header();
?>

	<main id="primary" class="site-main">
		<section class="hero">
			<img src="<?php echo get_template_directory_uri() . '/assets/images/maniken/top-circle.svg' ?>"
				 class="hero__image hero__image--top"/>

			<div class="hero__inner">
				<div class="mdc-layout-grid">
					<div class="mdc-layout-grid__inner">
						<div class="mdc-layout-grid__cell mdc-layout-grid__cell--span-4-tablet mdc-layout-grid__cell--span-5-desktop hero__image-wrapper">
							<img src="<?php echo get_template_directory_uri() . '/assets/images/maniken/mobile.svg' ?>"
								 class="d-n-t"/>
							<img src="<?php echo get_template_directory_uri() . '/assets/images/maniken/main.svg' ?>"
								 class="hero__image hero__image--main"/>
							<img src="<?php echo get_template_directory_uri() . '/assets/images/maniken/bottom-circle.svg' ?>"
								 class="hero__image hero__image--bottom"/>
						</div>
						<div class="mdc-layout-grid__cell mdc-layout-grid__cell--span-4-tablet mdc-layout-grid__cell--span-7-desktop">
							<h1>Авторское ателье<br/>TaRika для<br/>качественной жизни</h1>
						</div>
					</div>
				</div>
			</div>
		</section>

		<section class="about" id="about">
			<div class="mdc-layout-grid">
				<div class="mdc-layout-grid__inner">
					<div class="mdc-layout-grid__cell mdc-layout-grid__cell--span-1-tablet d-n d-b-t d-n-d"></div>
					<div class="mdc-layout-grid__cell mdc-layout-grid__cell--span-6-tablet d-n-d">
						<h2>О нас</h2>
					</div>
					<div class="mdc-layout-grid__cell mdc-layout-grid__cell--span-1-tablet d-n d-b-t d-n-d"></div>
					<div class="mdc-layout-grid__cell mdc-layout-grid__cell--span-1-tablet d-n d-b-t d-n-d"></div>
					<div class="mdc-layout-grid__cell mdc-layout-grid__cell--span-6-tablet mdc-layout-grid__cell--span-6-desktop about__image">
						<img src="http://tarika.com.ua/wp-content/uploads/2021/05/about-photo.png" alt="">
						<div class="about__content about__content--second d-n d-b-d">
							<div class="more__content" data-id="aboutText">
								<p>Ателье TаRika уникально тем, что здесь умеют слышать.  Мы из тех, кто понимает, что главное предназначение одежды — подружить глубинное внутреннее с внешним обликом. Мир сейчас так быстр. Дни пролетают с невообразимой скоростью. Люди, так часто не успевают сказать главного, извиниться за оплошность, пожелать добра и просто прожить свое «сейчас». Мы из тех, кто понимает истинное значение слова «служение» людям одного неба с нами. Нам интересно быть тихими участниками вашей жизни.</p>
								<p class="about__subtitle">До скорых встреч)</p>
							</div>
						</div>
					</div>
					<div class="mdc-layout-grid__cell mdc-layout-grid__cell--span-1-tablet d-n d-b-t d-n-d"></div>
					<div class="mdc-layout-grid__cell mdc-layout-grid__cell--span-1-tablet d-n d-b-t d-n-d"></div>
					<div class="mdc-layout-grid__cell mdc-layout-grid__cell--span-6-tablet mdc-layout-grid__cell--span-5-desktop about__content">
						<div class="mdc-layout-grid__inner">
							<div class="mdc-layout-grid__cell mdc-layout-grid__cell--span-2-desktop d-n d-b-d"></div>
							<div class="mdc-layout-grid__cell mdc-layout-grid__cell--span-10-desktop d-n d-b-d">
								<h2>О нас</h2>
							</div>
							<div class="mdc-layout-grid__cell mdc-layout-grid__cell--span-8-tablet mdc-layout-grid__cell--span-12-desktop">
								<p>Кто бы вы ни были, если вы на нашей страничке, значит вам интересна красота. Сократим
									это емкое понятие и поговорим о той красоте, в которую изо дня в день мы облекаем
									себя сами — нашей одежде.</p>
								<p>Давайте знакомиться: я Наталя Ричка — автор и создатель ателье TaRika. Мое увлечение,
									испытанное временем, в комплексе с образованием и опытом, на сегодняшний день
									позволяют предложить уникальную услугу — одевать вас с уважением. Уже более 20 лет
									востребованность авторских находок и конструкторско-технических решений —
									подтверждение того, что это удается.</p>
								<div class="more js-hook__more" data-target="aboutText">Читать дальше</div>
								<div class="more__content" data-id="aboutText">
									<p>К нам обращаются с абсолютно разными задачами:<br/>придумать и воплотить в жизнь свадебное/концертное платье; разработать корпоративную одежду; кардинально перекроив, вдохнуть новую жизнь в пальто/шубу; пошить шторы и другой домашний текстиль; перешить купленную через интернет (бывает, что заказанное нравится, но не сидит) или подаренную любимым мужем (мужчины иногда ошибаются с размером) вещь; подшить брюки, заштопать джинсы, реставрировать трикотаж или кожаные вещи, заменить пуговицы/молнии/кнопки и другую фурнитуру.<br/>В общем, разнообразить и привести в порядок ваш гардероб — это наша маленькая миссия.<br/>Нет необходимости рассказывать о том, что наши строчки ровные — постоянным клиентам известно качество исполнения, а новые, именно поэтому, становятся постоянными.</p>
									<p>Для людей понимающих, что внешний вид — отражение внутреннего состояния, культуры и вкуса, важны цвета и формы мельчайших деталей: ниток, строчек, пуговичек, крючков. Они обращают внимание на то, как обработана изнанка; для них важно с каким настроением их встретили, насколько внимательно выслушали и как глубоко вникли в суть просьб и пожеланий.</p>
									<p class="d-n-d">Ателье TаRika уникально тем, что здесь умеют слышать.  Мы из тех, кто понимает, что главное предназначение одежды — подружить глубинное внутреннее с внешним обликом. Мир сейчас так быстр. Дни пролетают с невообразимой скоростью. Люди, так часто не успевают сказать главного, извиниться за оплошность, пожелать добра и просто прожить свое «сейчас». Мы из тех, кто понимает истинное значение слова «служение» людям одного неба с нами. Нам интересно быть тихими участниками вашей жизни.</p>
									<p class="about__subtitle d-n-d">До скорых встреч)</p>
								</div>
							</div>
						</div>
					</div>
					<div class="mdc-layout-grid__cell mdc-layout-grid__cell--span-1-tablet mdc-layout-grid__cell--span-1-desktop d-n d-b-t"></div>
				</div>
			</div>
		</section>

		<section class="main-video">
			<div class="mdc-layout-grid">
				<div class="mdc-layout-grid__inner">
					<div class="mdc-layout-grid__cell mdc-layout-grid__cell--span-8-tablet mdc-layout-grid__cell--span-12-desktop">
						<div class="d-n d-b-t">
							<?php echo do_shortcode( '[video poster="http://tarika.com.ua/wp-content/uploads/2021/03/main-video-poster.jpg" src="http://tarika.com.ua/wp-content/uploads/2021/05/5533-1.mp4" preload="none"]' ); ?>
						</div>
						<div class="d-n-t">
							<?php echo do_shortcode( '[video poster="http://tarika.com.ua/wp-content/uploads/2021/03/main-video-poster.jpg" src="http://tarika.com.ua/wp-content/uploads/2021/03/0-02-0a-983816736e43ea706ce497589eaefb424b13c480cc1c076244d4f98d082f3729_1c6da4f8639ee8.mp4" preload="none"]' ); ?>
						</div>
					</div>
				</div>
			</div>
		</section>

		<section class="services" id="services">
			<img src="<?php echo get_template_directory_uri() . '/assets/images/services-illustration.svg' ?>"
				 class="services__image"
				 alt="Илюстрация">
			<div class="mdc-layout-grid">
				<div class="mdc-layout-grid__inner">
					<div class="mdc-layout-grid__cell mdc-layout-grid__cell--span-1-tablet mdc-layout-grid__cell--span-5-desktop d-n d-b-t"></div>
					<div class="mdc-layout-grid__cell mdc-layout-grid__cell--span-7-tablet mdc-layout-grid__cell--span-7-desktop">
						<h2>Услуги ателье</h2>
					</div>
					<div class="mdc-layout-grid__cell mdc-layout-grid__cell--span-2-tablet mdc-layout-grid__cell--span-6-desktop d-n d-b-t"></div>
					<div class="services__content mdc-layout-grid__cell mdc-layout-grid__cell--span-6-tablet mdc-layout-grid__cell--span-6-desktop">
						<ul>
							<li>индивидуальный пошив одежды;</li>
							<li>дизайнерская разработка моделей любой сложности, творческое переосмысление устаревших
								вещей, разработка корпоративной одежды, сценических костюмов;
							</li>
							<li>ремонт и реставрация одежды;</li>
							<li>услуги личного портного<br/>(выезд на дом и т.п.);</li>
							<li>пошив и дизайн штор, другого домашнего текстиля;</li>
							<li>химчистка.</li>
						</ul>

						<div class="mdc-layout-grid__inner">
							<div class="mdc-layout-grid__cell mdc-layout-grid__cell--span-1-tablet mdc-layout-grid__cell--span-2-desktop d-n d-b-t"></div>
							<div class="mdc-layout-grid__cell mdc-layout-grid__cell--span-7-tablet mdc-layout-grid__cell--span-10-desktop">
								<footer class="services__actions">
									<a href="<?php echo get_site_url() . '/price'; ?>" class="button button--icon">
										<svg version="1.1" xmlns="http://www.w3.org/2000/svg"
											 xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="25px"
											 height="20px" viewBox="0 0 25 20"
											 xml:space="preserve">
									<path class="st0" d="M5.5,8.4v3.2c0,0.5-0.5,1-1,1H1c-0.6,0-1-0.4-1-1V8.4c0-0.5,0.5-1,1-1h3.4C5,7.4,5.5,7.8,5.5,8.4z M24,7.4H9.2
	H8.6c-0.6,0-1,0.4-1,1v3.2c0,0.5,0.5,1,1,1h0.5H24c0.6,0,1-0.4,1-1V8.4C25,7.8,24.5,7.4,24,7.4z M4.5,0H1C0.5,0,0,0.4,0,1v3.2
	c0,0.5,0.5,1,1,1h3.4c0.6,0,1-0.4,1-1V1C5.5,0.4,5,0,4.5,0z M24,0H9.2H8.6c-0.6,0-1,0.4-1,1v3.2c0,0.5,0.5,1,1,1h0.5H24
	c0.6,0,1-0.4,1-1V1C25,0.4,24.5,0,24,0z M4.5,14.8H1c-0.6,0-1,0.4-1,1V19c0,0.5,0.5,1,1,1h3.4c0.6,0,1-0.4,1-1v-3.2
	C5.5,15.2,5,14.8,4.5,14.8z M24,14.8H9.2H8.6c-0.6,0-1,0.4-1,1V19c0,0.5,0.5,1,1,1h0.5H24c0.6,0,1-0.4,1-1v-3.2
	C25,15.2,24.5,14.8,24,14.8z"/>
</svg>

										<span class="button-text">Посмотреть прайс</span>
									</a>
									<?php if ( ! empty( get_option( 'work_phone' ) ) ): ?>
										<a href="tel:<?php echo esc_attr( get_option( 'work_phone' ) ) ?>"
										   class="icon-phone">
											<svg width="40" height="40" viewBox="0 0 40 40" fill="none"
												 xmlns="http://www.w3.org/2000/svg">
												<path d="M39.1032 31.6117L32.9258 25.4336C31.6954 24.2081 29.6578 24.2454 28.3845 25.5191L25.2723 28.6306C25.0757 28.5222 24.8722 28.4091 24.6582 28.289C22.6929 27.2 20.003 25.7075 17.1725 22.8749C14.3336 20.0363 12.8397 17.3423 11.7474 15.3757C11.6321 15.1674 11.5217 14.9665 11.4126 14.7757L13.5014 12.6901L14.5283 11.662C15.8035 10.3864 15.8387 8.34935 14.6111 7.12031L8.43374 0.941566C7.2062 -0.285748 5.16769 -0.248515 3.89245 1.02706L2.15146 2.77806L2.19903 2.82529C1.61525 3.57018 1.12743 4.4293 0.764407 5.35576C0.429769 6.23764 0.221424 7.07917 0.126158 7.92243C-0.689524 14.6846 2.4006 20.8647 10.7868 29.251C22.379 40.8425 31.7209 39.9668 32.1239 39.9241C33.0016 39.8192 33.8428 39.6094 34.6977 39.2775C35.6161 38.9187 36.4747 38.4315 37.2191 37.849L37.2571 37.8828L39.0209 36.1557C40.2935 34.8804 40.3301 32.8427 39.1032 31.6117Z"
													  fill="white"/>
											</svg>
										</a>
									<?php endif; ?>
								</footer>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>

<?php if ( get_option( 'include_our_works' ) ): ?>
		<section class="works-slider" id="works">
			<div class="mdc-layout-grid">
				<div class="mdc-layout-grid__inner">
					<div class="mdc-layout-grid__cell mdc-layout-grid__cell--span-8-tablet mdc-layout-grid__cell--span-12-desktop">
						<h2 class="works-slider__title">Наши работы</h2>
					</div>
				</div>
			</div>
			<div class="works-slider__inner js-hook__works-slider">
				<div class="works-slide">
					<div class="works-slide__image">
						<img src="http://tarika.com.ua/wp-content/uploads/2021/05/photo-1575929065444-29c8b0e9c895.jpeg" alt="1"/>
					</div>
					<div class="works-slide__date">
						<h4>08 / 2019</h4>
					</div>
				</div>
				<div class="works-slide">
					<div class="works-slide__image">
						<img src="http://tarika.com.ua/wp-content/uploads/2021/05/photo-1485462537746-965f33f7f6a7.jpeg" alt="2"/>
					</div>
					<div class="works-slide__date">
						<h4>09 / 2019</h4>
					</div>
				</div>
				<div class="works-slide">
					<div class="works-slide__image">
						<img src="http://tarika.com.ua/wp-content/uploads/2021/05/tai-s-captures-u-MwguPdm7A-unsplash.jpeg" alt="3"/>
					</div>
					<div class="works-slide__date">
						<h4>10 / 2019</h4>
					</div>
				</div>
		</section>
<?php endif;?>

<?php if ( get_option( 'include_shop' ) ): ?>
		<section class="shop-slider" id="shop">
			<img class="shop-slider__image"
				 src="<?php echo get_stylesheet_directory_uri() . '/assets/images/shop-slider-line.svg' ?>" alt="">

			<div class="mdc-layout-grid">
				<div class="mdc-layout-grid__inner">
					<div class="mdc-layout-grid__cell mdc-layout-grid__cell--span-2-tablet d-n d-b-t  d-n-d"></div>
					<div class="mdc-layout-grid__cell mdc-layout-grid__cell--span-6-tablet mdc-layout-grid__cell--span-12-desktop">
						<h2>Магазин</h2>
					</div>
				</div>
			</div>

			<div class="mdc-layout-grid">
				<div class="mdc-layout-grid__inner">
					<div class="mdc-layout-grid__cell mdc-layout-grid__cell--span-1-tablet d-n d-b-t d-n-d"></div>
					<div class="mdc-layout-grid__cell mdc-layout-grid__cell--span-7-tablet mdc-layout-grid__cell--span-12-desktop shop-slider__inner">
						<div class="mdc-layout-grid__inner">
							<div class="mdc-layout-grid__cell mdc-layout-grid__cell--span-1-phone mdc-layout-grid__cell--span-1-tablet mdc-layout-grid__cell--span-2-desktop"></div>
							<div class="mdc-layout-grid__cell mdc-layout-grid__cell--span-3-phone mdc-layout-grid__cell--span-7-tablet mdc-layout-grid__cell--span-10-desktop shop-slider__column">
								<div class="js-hook_shop-slider">
									<div class="shop-slide">
										<div class="shop-slide__image">
											<img src="http://tarika.com.ua/wp-content/uploads/2021/05/photo-1575929065444-29c8b0e9c895.jpeg"
												 alt="1"/>
										</div>
										<p class="shop-slide__name">Платье</p>
										<h4 class="shop-slide__price">1254 грн</h4>
									</div>
									<div class="shop-slide">
										<div class="shop-slide__image">
											<img src="http://tarika.com.ua/wp-content/uploads/2021/05/photo-1485462537746-965f33f7f6a7.jpeg"
												 alt="2"/>
										</div>
										<p class="shop-slide__name">Пиджак</p>
										<h4 class="shop-slide__price">840 грн</h4>
									</div>
									<div class="shop-slide">
										<div class="shop-slide__image">
											<img src="http://tarika.com.ua/wp-content/uploads/2021/05/tai-s-captures-u-MwguPdm7A-unsplash.jpeg"
												 alt="3"/>
										</div>
										<p class="shop-slide__name">Пальто</p>
										<h4 class="shop-slide__price">1540 грн</h4>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
<?php endif;?>

		<section class="circle-list">
			<div class="mdc-layout-grid">
				<div class="mdc-layout-grid__inner">
					<div class="mdc-layout-grid__cell mdc-layout-grid__cell--span-8-tablet mdc-layout-grid__cell--span-12-desktop">
						<div class="circle-list-item" id="dry_cleaning">
							<div class="circle-list-item__placeholder">
								<div class="circle-list-item__icon circle-list-item__icon--blue circle-list-item__icon--first">
									<img src="<?php echo get_stylesheet_directory_uri() . '/assets/images/dry-cleaning-icon.svg' ?>"
										 alt="Dry Cleaning Icon">
								</div>
							</div>
							<div class="circle-list-item__content">
								<h2 class="circle-list-item__title"><a href="/dry-cleaning/">Хи&#173;мчи&#173;ст&#173;ка</a></h2>
								<p class="circle-list-item__copy"><a href="/dry-cleaning/">Вопросы идущих к нам людей связаны не только<br class="d-n d-b-t"/> с пошивом и ремонтом одежды. Мы также с удовольствием примем у вас вещи в  химчистку...</a></p>
							</div>
						</div>
						<div class="circle-list-item" id="charity">
							<div class="circle-list-item__placeholder">
								<div class="circle-list-item__icon circle-list-item__icon--orange circle-list-item__icon--second">
									<img src="<?php echo get_stylesheet_directory_uri() . '/assets/images/charity-icon.svg' ?>"
										 alt="Charity Icon">
								</div>
							</div>
							<div class="circle-list-item__content">
								<h2 class="circle-list-item__title"><a href="/charity/">Благотвори&#173;тельность</a></h2>
								<p class="circle-list-item__copy"><a href="/charity/">Тему благих дел, мы не станем освещать долго и подробно.<br class="d-n d-b-t"/> В этом разделе стоит задача рассказать, как это можно делать<br class="d-n d-b-t"/> в нашем ателье. Сегодня можем предложить два варианта...</a></p>
							</div>
						</div>
						<div class="circle-list-item" id="learning">
							<div class="circle-list-item__placeholder">
								<div class="circle-list-item__icon circle-list-item__icon--red circle-list-item__icon--third">
									<img src="<?php echo get_stylesheet_directory_uri() . '/assets/images/tools-icon.svg' ?>"
										 alt="Tools Icon">
								</div>
							</div>
							<div class="circle-list-item__content">
								<h2 class="circle-list-item__title"><a href="/training/">Обу&#173;че&#173;ние</a></h2>
								<p class="circle-list-item__copy"><a href="/training/">За 20 лет работы в любой сфере, знаний и опыта накапливается более чем достаточно, чтобы начать ими уверенно делиться. Мы будем говорить о себе и желании обучать шитью...</a></p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>

<?php if ( get_option( 'include_portfolio' ) ): ?>
		<section class="articles" id="articles">
			<img src="<?php echo get_stylesheet_directory_uri() . '/assets/images/articles-illustration-red.svg' ?>"
				 alt="Red box illustration"
				 class="articles__image-red d-n d-b-t"/>
			<img src="<?php echo get_stylesheet_directory_uri() . '/assets/images/articles-illustration-grey.svg' ?>"
				 alt="Grey box illustration"
				 class="articles__image-grey d-n d-b-d"/>
			<img src="<?php echo get_stylesheet_directory_uri() . '/assets/images/articles-illustration-yellow.svg' ?>"
				 alt="Yellow box illustration"
				 class="articles__image-yellow d-n d-b-t"/>
			<img src="<?php echo get_stylesheet_directory_uri() . '/assets/images/articles-illustration-line.svg' ?>"
				 alt="Line box illustration"
				 class="articles__image-line d-n d-b-t"/>
			<div class="mdc-layout-grid">
				<div class="mdc-layout-grid__inner">
					<div class="mdc-layout-grid__cell mdc-layout-grid__cell--span-8-tablet mdc-layout-grid__cell--span-12-desktop">
						<header class="articles__header">
							<h3>Мы рассказываем <br class="d-n-t"/>о том что знаем</h3>
							<a href="" class="more">Больше статей</a>
						</header>
					</div>
				</div>
				<div class="mdc-layout-grid__inner articles__row">
					<div class="mdc-layout-grid__cell mdc-layout-grid__cell--span-1-desktop d-n d-b-d"></div>
					<div class="mdc-layout-grid__cell mdc-layout-grid__cell--span-8-tablet mdc-layout-grid__cell--span-10-desktop">
						<div class="articles__list">
							<a class="article-card">
								<div class="article-card__image">
									<img src="http://tarika.com.ua/wp-content/uploads/2021/05/Rectangle-79.png" alt="Article 1"/>
								</div>
								<div class="article-card__content">
									<h2 class="article-card__title">Что делать, если нечего надеть и некуда повесить?</h2>
									<p>Решаем извечную женскую проблему. Что делать, если надеть нечего и повесить некуда? Причины и пути решения...</p>
								</div>
							</a>

							<a class="article-card">
								<div class="article-card__image">
									<img src="http://tarika.com.ua/wp-content/uploads/2021/05/Rectangle-79.png" alt="Article 2"/>
								</div>
								<div class="article-card__content">
									<h2 class="article-card__title">Вертикали, горизонтали и их влияние на силуэт</h2>
									<p>Продолжаем серию постов о зрительных иллюзиях в одежде...</p>
								</div>
							</a>

							<a class="article-card">
								<div class="article-card__image">
									<img src="http://tarika.com.ua/wp-content/uploads/2021/05/Rectangle-85.png" alt="Article 3"/>
								</div>
								<div class="article-card__content">
									<h2 class="article-card__title">Брошкино путешествие</h2>
									<p>Брошь: чем она кардинально отличается от других аксессуаров?...</p>
								</div>
							</a>
						</div>
					</div>
					<div class="mdc-layout-grid__cell mdc-layout-grid__cell--span-1-desktop d-n d-b-d"></div>
				</div>
			</div>
		</section>
<?php endif; ?>

		<section class="benefits">
			<div class="mdc-layout-grid">
				<div class="mdc-layout-grid__inner">
					<div class="mdc-layout-grid__cell mdc-layout-grid__cell--span-8-tablet mdc-layout-grid__cell--span-12-desktop">
						<h2>Наши преимущества</h2>
						<h3 class="benefits__subtitle"><i>Обычно в этом разделе себя хвалят.<br/>Мы же расскажем о том, что слышим каждый день:</i></h3>
					</div>
				</div>
			</div>

			<div class="quote">
				<img src="<?php echo get_stylesheet_directory_uri() . '/assets/images/quote-icon.svg'?>"
					 class="quote__icon"
					 alt="Quote icon"/>

				<div class="mdc-layout-grid">
					<div class="mdc-layout-grid__inner">
						<div class="mdc-layout-grid__cell mdc-layout-grid__cell--span-1-tablet mdc-layout-grid__cell--span-2-desktop d-n d-b-t"></div>

						<div class="mdc-layout-grid__cell mdc-layout-grid__cell--span-6-tablet mdc-layout-grid__cell--span-8-desktop">
							<div class="mdc-layout-grid__inner quote__inner">
								<div class="mdc-layout-grid__cell  mdc-layout-grid__cell--span-1-desktop d-n d-b-d"></div>

								<div class="mdc-layout-grid__cell  mdc-layout-grid__cell--span-8-tablet mdc-layout-grid__cell--span-10-desktop">
									<blockqoute>
										<p class="quote__item">Я думала это платье спасти невозможно!</p>
										<p class="quote__item">На невозможное просто нужно немного больше времени.<br/>Мы действительно любим свою работу, а качество для нас - синоним любви.</p>
										<p class="quote__item">Люблю к вам приходить. Вы всегда услышите и объясните. Результат — новая/обновленная вещь и вдохновленная я.</p>
										<p class="quote__item">Слышать учимся ежедневно, а наш опыт позволяет предложить вам больше, чем вы ожидали. Результат — совершенствуемся мы, довольны вы.</p>
										<p class="quote__item">Мне нравится ваш подход. К вам можно с любыми вопросами.</p>
										<p class="quote__item">Все, кто к нам пришел — особенные люди. Особенным людям — особый подход.</p>
									</blockqoute>
								</div>

								<div class="mdc-layout-grid__cell  mdc-layout-grid__cell--span-1-desktop d-n d-b-d"></div>
							</div>
						</div>

						<div class="mdc-layout-grid__cell mdc-layout-grid__cell--span-1-tablet mdc-layout-grid__cell--span-2-desktop d-n d-b-t"></div>
					</div>
				</div>
			</div>

			<div class="mdc-layout-grid">
				<div class="mdc-layout-grid__inner">
					<div class="mdc-layout-grid__cell mdc-layout-grid__cell--span-1-tablet mdc-layout-grid__cell--span-2-desktop d-n d-b-t"></div>
					<div class="mdc-layout-grid__cell mdc-layout-grid__cell--span-6-tablet mdc-layout-grid__cell--span-8-desktop">
						<footer class="benefits__footer">
							<h3><i>Каждодневный труд, приносящий удовольствие — большая жизненная удача. Мы удачливы. Приходите, будем рады!</i></h3>
						</footer>
					</div>
					<div class="mdc-layout-grid__cell mdc-layout-grid__cell--span-1-tablet mdc-layout-grid__cell--span-2-desktop d-n d-b-t"></div>
				</div>
			</div>
		</section>
	</main>

<?php
get_footer();

