<?php
/*
Template Name: General Wrapper
*/

get_header();
?>
	<main id="primary" class="site-main">
		<section class="section-indent">
			<div class="mdc-layout-grid">
				<div class="mdc-layout-grid__inner">
					<div class="mdc-layout-grid__cell mdc-layout-grid__cell--span-1-tablet mdc-layout-grid__cell--span-2-desktop d-n d-b-t"></div>
					<div class="mdc-layout-grid__cell mdc-layout-grid__cell--span-7-tablet mdc-layout-grid__cell--span-10-desktop">
						<h1 class="section-title"><?php echo get_the_title(); ?></h1>
					</div>
				</div>
				<div class="mdc-layout-grid__inner">
					<div class="mdc-layout-grid__cell mdc-layout-grid__cell--span-7-tablet mdc-layout-grid__cell--span-12-desktop">
						<div class="section-content">
							<?php the_content(); ?>
						</div>
					</div>
				</div>
			</div>
		</section>
	</main>
<?php

get_footer();
