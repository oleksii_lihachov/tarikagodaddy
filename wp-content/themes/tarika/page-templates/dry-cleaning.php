<?php
/*
Template Name: Dry Cleaning
*/

get_header();
?>
	<main id="primary" class="site-main">
		<section class="dry-cleaning section-indent">
			<div class="mdc-layout-grid">
				<div class="mdc-layout-grid__inner">
					<div class="mdc-layout-grid__cell mdc-layout-grid__cell--span-1-tablet mdc-layout-grid__cell--span-2-desktop d-n d-b-t"></div>
					<div class="mdc-layout-grid__cell mdc-layout-grid__cell--span-7-tablet mdc-layout-grid__cell--span-10-desktop">
						<h1><?php echo get_the_title(); ?></h1>
					</div>
				</div>
				<div class="dry-cleaning__content mdc-layout-grid__inner">

					<div class="dry-cleaning__image mdc-layout-grid__cell mdc-layout-grid__cell--span-4-tablet mdc-layout-grid__cell--span-6-desktop">
						<?php echo get_the_post_thumbnail(); ?>
					</div>

					<div class="mdc-layout-grid__cell mdc-layout-grid__cell--span-4-tablet mdc-layout-grid__cell--span-6-desktop">

						<div class="mdc-layout-grid__inner">
							<div class="mdc-layout-grid__cell mdc-layout-grid__cell--span-7-tablet mdc-layout-grid__cell--span-10-desktop">
								<p>Вопросы идущих к нам людей связаны не только с пошивом и ремонтом одежды. Мы также с
									удовольствием примем у вас вещи в химчистку. Конечно же, мы делаем это не сами, ведь
									профессиональная чистка — это целое производство. А вот принять-выдать то, что вы
									хотите освежить — это к нам.</p>
							</div>
							<div class="mdc-layout-grid__cell mdc-layout-grid__cell--span-1-tablet mdc-layout-grid__cell--span-2-desktop d-n d-b-t"></div>
						</div>

						<div class="mdc-layout-grid__inner">
							<div class="mdc-layout-grid__cell mdc-layout-grid__cell--span-1-tablet mdc-layout-grid__cell--span-2-desktop d-n d-b-t"></div>
							<div class="mdc-layout-grid__cell mdc-layout-grid__cell--span-7-tablet mdc-layout-grid__cell--span-8-desktop">
								<p class="dry-cleaning__text">Поскольку мы дорожим своей репутацией, то старались найти
									партнеров с нашим
									пониманием соответствия цены-качества. Уже долгое время результаты нашего
									сотрудничества с химчисткой «4 сезона» подтверждают — выбор сделан правильно. Ребята
									настолько щепетильно и добросовестно относятся к самому процессу и к сроку его
									исполнения, что на сегодняшний день, мы можем рекомендовать обращаться именно к
									ним.</p>
								<div class="dry-cleaning__actions">
									<a href="/price" class="button button--icon">
										<svg version="1.1" xmlns="http://www.w3.org/2000/svg"
											 xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="25px"
											 height="20px" viewBox="0 0 25 20" xml:space="preserve">
												<path class="st0" d="M5.5,8.4v3.2c0,0.5-0.5,1-1,1H1c-0.6,0-1-0.4-1-1V8.4c0-0.5,0.5-1,1-1h3.4C5,7.4,5.5,7.8,5.5,8.4z M24,7.4H9.2
													H8.6c-0.6,0-1,0.4-1,1v3.2c0,0.5,0.5,1,1,1h0.5H24c0.6,0,1-0.4,1-1V8.4C25,7.8,24.5,7.4,24,7.4z M4.5,0H1C0.5,0,0,0.4,0,1v3.2
													c0,0.5,0.5,1,1,1h3.4c0.6,0,1-0.4,1-1V1C5.5,0.4,5,0,4.5,0z M24,0H9.2H8.6c-0.6,0-1,0.4-1,1v3.2c0,0.5,0.5,1,1,1h0.5H24
													c0.6,0,1-0.4,1-1V1C25,0.4,24.5,0,24,0z M4.5,14.8H1c-0.6,0-1,0.4-1,1V19c0,0.5,0.5,1,1,1h3.4c0.6,0,1-0.4,1-1v-3.2
													C5.5,15.2,5,14.8,4.5,14.8z M24,14.8H9.2H8.6c-0.6,0-1,0.4-1,1V19c0,0.5,0.5,1,1,1h0.5H24c0.6,0,1-0.4,1-1v-3.2
													C25,15.2,24.5,14.8,24,14.8z"></path>
										</svg>
										<span class="button-text">Посмотреть прайс</span>
									</a>
								</div>
							</div>
							<div class="mdc-layout-grid__cell mdc-layout-grid__cell--span-2-desktop d-n d-b-d"></div>
						</div>

					</div>
				</div>

				<div class="mdc-layout-grid__inner">
					<div class="mdc-layout-grid__cell mdc-layout-grid__cell--span-1-desktop d-n d-b-d"></div>
					<div class="mdc-layout-grid__cell mdc-layout-grid__cell--span-7-tablet mdc-layout-grid__cell--span-11-desktop">
						<div class="mini-contacts">
							<p>Вещи мы примем по адресу:<br><strong>г. Киев, просп. Петра Григоренка, 38А, <span
											class="no-wrap">офис
										86</span></strong></p>
							<a href="/contacts" class="more no-wrap">Посмотреть карту</a>
						</div>
					</div>
				</div>

				<div class="mdc-layout-grid__inner">
					<div class="mdc-layout-grid__cell mdc-layout-grid__cell--span-8-tablet mdc-layout-grid__cell--span-12-desktop">
						<footer class="dry-cleaning__footer"><h3>Приходите, приносите, чисто жить приятно!</h3></footer>
					</div>
				</div>
			</div>
		</section>
	</main>
<?php

get_footer();
