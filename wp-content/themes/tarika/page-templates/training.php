<?php
/*
Template Name: Training
*/

get_header();
?>
	<main id="primary" class="site-main">
		<section class="training section-indent">
			<div class="mdc-layout-grid">

				<div class="mdc-layout-grid__inner">
					<div class="mdc-layout-grid__cell mdc-layout-grid__cell--span-1-tablet mdc-layout-grid__cell--span-2-desktop d-n d-b-t"></div>
					<div class="mdc-layout-grid__cell mdc-layout-grid__cell--span-7-tablet mdc-layout-grid__cell--span-10-desktop">
						<h1><?php echo get_the_title(); ?></h1>
					</div>
				</div>

				<div class="mdc-layout-grid__inner">
					<div class="mdc-layout-grid__cell mdc-layout-grid__cell--span-3-desktop d-n d-b-d"></div>
					<div class="mdc-layout-grid__cell mdc-layout-grid__cell--span-6-tablet mdc-layout-grid__cell--span-6-desktop">
						<h3 class="training__subtitle">«Великое искусство научиться многому —<br class="d-n d-i-t"/> это
							браться сразу за
							немногое.»<br/>Джон Локк</h3>
					</div>
					<div class="mdc-layout-grid__cell mdc-layout-grid__cell--span-2-tablet mdc-layout-grid__cell--span-3-desktop d-n d-b-t"></div>
				</div>

				<div class="mdc-layout-grid__inner">

					<div class="mdc-layout-grid__cell mdc-layout-grid__cell--span-5-tablet mdc-layout-grid__cell--span-6-desktop">

						<div class="mdc-layout-grid__inner">
							<div class="mdc-layout-grid__cell mdc-layout-grid__cell--span-8-tablet mdc-layout-grid__cell--span-10-desktop">
								<p>За 20 лет работы в любой сфере, знаний и опыта накапливается более чем достаточно,
									чтобы начать ими уверенно делиться. Мы будем говорить о себе и желании обучать
									шитью. Неоспоримым плюсом в нашем случае является то, что качественно оборудованное
									ателье «TaRika» дает возможность подкреплять теорию практикой.</p>
							</div>
							<div class="mdc-layout-grid__cell mdc-layout-grid__cell--span-2-desktop d-n d-b-d"></div>
						</div>

						<div class="mdc-layout-grid__inner">
							<div class="mdc-layout-grid__cell mdc-layout-grid__cell--span-2-desktop d-n d-b-d"></div>

							<div class="mdc-layout-grid__cell mdc-layout-grid__cell--span-8-tablet mdc-layout-grid__cell--span-9-desktop">
								<p class="training__text">Вышеуказанные обстоятельства способствовали появлению курсов
									швейного дела. <br/>На
									сегодняшний день вашему вниманию:</p>
								<ul class="training__list">
									<li>группы 3-4 человека по каждому направлению;</li>
									<li>занятия по воскресеньям (длительность 5 часов с небольшим перерывом), в
										действующем ателье со всем необходимым оборудованием и инструментами;
									</li>
									<li>в конце курса каждый имеет на руках пошитое им изделие, соответствующее
										программе.
									</li>
								</ul>
							</div>

							<div class="mdc-layout-grid__cell mdc-layout-grid__cell--span-1-desktop d-n d-b-d"></div>
						</div>

						<div class="mdc-layout-grid__inner">
							<div class="mdc-layout-grid__cell mdc-layout-grid__cell--span-8-tablet mdc-layout-grid__cell--span-10-desktop">
								<p class="training__text">Главным в предлагаемых курсах является то, что обучение
									направлено на реальную жизнь.
									Получая навыки швейного мастерства, каждый имеет возможность рассматривать их и как
									хобби, и как возможность одеть себя и близких, и как дополнительный источник дохода.
									Для
									скептиков, утверждающих, что «...нынче профессия портного не в тренде...», есть
									весомый
									аргумент: «Для нас успех — жить своей жизнью. Мы можем позволить себе роскошь — не
									работать ни одного дня, любимое дело тому причина».</p>
							</div>
							<div class="mdc-layout-grid__cell mdc-layout-grid__cell--span-2-desktop d-n d-b-d"></div>
						</div>

						<div class="mdc-layout-grid__inner">
							<div class="mdc-layout-grid__cell mdc-layout-grid__cell--span-8-tablet mdc-layout-grid__cell--span-12-desktop">
								<footer class="training__footer">
									<h3>Возможно вы искали нас?</h3>
									<?php if ( ! empty( get_option( 'studio_phone' ) ) ): ?>
										<a href="tel:<?php echo esc_attr( get_option( 'studio_phone' ) ) ?>"
										   class="button button--icon">
											<svg width="25" height="26" viewBox="0 0 25 26" fill="none"
												 xmlns="http://www.w3.org/2000/svg">
												<path d="M12.5 0.245117C5.60722 0.245117 0 5.96677 0 13.0002C0 20.0337 5.60722 25.7553 12.5 25.7553C19.3928 25.7553 25 20.0337 25 13.0002C25 5.96677 19.3928 0.245117 12.5 0.245117ZM18.8354 10.2972L12.0646 17.2061C11.8614 17.4134 11.5948 17.5177 11.3281 17.5177C11.0615 17.5177 10.7948 17.4134 10.5917 17.2061L7.20634 13.7517C6.79893 13.3361 6.79893 12.6643 7.20634 12.2488C7.61356 11.833 8.27179 11.833 8.6792 12.2488L11.3281 14.9517L17.3626 8.79432C17.7698 8.3786 18.428 8.3786 18.8354 8.79432C19.2427 9.20985 19.2427 9.88151 18.8354 10.2972Z"
													  fill="#101F32"/>
											</svg>
											<span class="button-text">Записаться на курс</span>
										</a>
									<?php endif; ?>
								</footer>
							</div>
						</div>
					</div>

					<div class="mdc-layout-grid__cell mdc-layout-grid__cell--span-3-tablet mdc-layout-grid__cell--span-6-desktop">
						<div class="training__image">
							<?php echo get_the_post_thumbnail(); ?>
						</div>
					</div>
				</div>
			</div>
		</section>
	</main>
<?php

get_footer();
