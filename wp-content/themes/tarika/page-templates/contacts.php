<?php
/*
Template Name: Contacts
*/

get_header();

$fb_link    = get_option( 'facebook_link' );
$insta_link = get_option( 'instagram_link' );
$work_phone = get_option( 'work_phone' );
$work_email = get_option( 'work_email' );
?>
	<main id="primary" class="site-main">
		<section class="contacts">
			<div class="mdc-layout-grid">
				<div class="mdc-layout-grid__inner">
					<div class="mdc-layout-grid__cell mdc-layout-grid__cell--span-4-tablet mdc-layout-grid__cell--span-4-desktop">
						<div class="mdc-layout-grid__inner">
							<div class="mdc-layout-grid__cell mdc-layout-grid__cell--span-8-tablet mdc-layout-grid__cell--span-12-desktop">
								<h1><?php echo get_the_title(); ?></h1>
							</div>
							<div class="mdc-layout-grid__cell mdc-layout-grid__cell--span-2-desktop"></div>
							<div class="mdc-layout-grid__cell mdc-layout-grid__cell--span-8-tablet mdc-layout-grid__cell--span-10-desktop">
								<div class="contact-info">
									<p><strong>Адрес:</strong><br/>Киев, просп. Петра Григоренка, 38А, офис 86</p>
									<p><strong>Телефоны:</strong><br/>Ателье, химчистка<br/>(099) 669 46 76</p>
									<p>Магазин, обучение, благотворительность<br/>(067) 39 51 846</p>
									<p><strong>Мы работаем:</strong><br/>ПН-ПТ 9:00-21:00<br/>СБ 10:00-19:00</p>
								</div>
								<div class="socials socials--left">
									<?php if ( ! empty( $fb_link ) ): ?>
										<a href="<?php echo esc_url( $fb_link ); ?>"
										   class="socials__item"
										   target="_blank">
											<img src="<?php echo get_template_directory_uri() . '/assets/images/socials/facebook.svg' ?>"
												 alt="Facebook icon"/>
										</a>
									<?php endif; ?>
									<?php if ( ! empty( $insta_link ) ): ?>
										<a href="<?php echo esc_url( $insta_link ); ?>"
										   class="socials__item"
										   target="_blank">
											<img src="<?php echo get_template_directory_uri() . '/assets/images/socials/instagram.svg' ?>"
												 alt="Instagram icon"/>
										</a>
									<?php endif; ?>
									<?php if ( ! empty( $work_phone ) ): ?>
										<a href="tel:<?php echo esc_attr( $work_phone ); ?>"
										   class="socials__item">
											<img src="<?php echo get_template_directory_uri() . '/assets/images/socials/viber.svg' ?>"
												 alt="Viber icon"/>
										</a>
									<?php endif; ?>
									<?php if ( ! empty( $work_email ) ): ?>
										<a href="mailto:<?php echo esc_attr( get_option( 'work_email' ) ); ?>"
										   class="socials__item">
											<img src="<?php echo get_template_directory_uri() . '/assets/images/socials/email.svg' ?>"
												 alt="Email icon"/>
										</a>
									<?php endif; ?>
								</div>
							</div>
						</div>
					</div>
					<div class="mdc-layout-grid__cell mdc-layout-grid__cell--span-4-tablet mdc-layout-grid__cell--span-8-desktop g-map">
						<div class="g-map__wrapper">
							<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d10174.359627333311!2d30.631773!3d50.3928429!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xadec52ecda0e88ad!2z0JDRgtC10LvRjNGUIFRhUmlrYQ!5e0!3m2!1sru!2sua!4v1614502470241!5m2!1sru!2sua"
									frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false"
									tabindex="0"></iframe>
						</div>
					</div>
				</div>
			</div>
		</section>
	</main>
<?php
get_footer();
