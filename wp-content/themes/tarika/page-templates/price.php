<?php
/*
Template Name: Price
*/

get_header();
?>
<main id="primary" class="site-main">
    <section class="price">
        <div class="mdc-layout-grid">
            <div class="mdc-layout-grid__inner">
                <div
                    class="mdc-layout-grid__cell mdc-layout-grid__cell--span-8-tablet mdc-layout-grid__cell--span-12-desktop">
                    <div class="breadcrumbs">
                        <ul class="breadcrumbs__list">
                            <li class="breadcrumbs__item"><a href="<?php echo get_site_url(); ?>">Главная</a></li>
                            <li class="breadcrumbs__item"><a href="<?php echo get_site_url(); ?>/#services">Услуги
                                    ателье</a></li>
                            <li class="breadcrumbs__item">Прайс</li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="mdc-layout-grid__inner price__inner">
                <div
                    class="mdc-layout-grid__cell mdc-layout-grid__cell--span-1-tablet mdc-layout-grid__cell--span-2-desktop d-n d-b-t">
                </div>
                <div
                    class="mdc-layout-grid__cell mdc-layout-grid__cell--span-6-tablet mdc-layout-grid__cell--span-7-desktop">
                    <ul class="accordion js-hook__accordion">
                        <li class="accordion__item">
                            <h2 class="title">
                                Индивидуальный пошив одежды
                            </h2>
                            <div class="content">
                                <div class="mdc-layout-grid__inner">
                                    <div
                                        class="mdc-layout-grid__cell mdc-layout-grid__cell--span-1-tablet mdc-layout-grid__cell--span-2-desktop d-n d-b-t">
                                    </div>
                                    <div
                                        class="mdc-layout-grid__cell mdc-layout-grid__cell--span-7-tablet mdc-layout-grid__cell--span-10-desktop">
                                        <ul class="price__list">
                                            <li class="price__item">
                                                <span>Юбка, брюки женские</span>
                                                <span>1800-4000</span>
                                            </li>
                                            <li class="price__item">
                                                <span>Юбка, брюки (сложное моделирование)</span>
                                                <span>3000-5000</span>
                                            </li>
                                            <li class="price__item">
                                                <span>Брюки мужские классические</span>
                                                <span>4000-5000</span>
                                            </li>
                                            <li class="price__item">
                                                <span>Шорты (ж/м)</span>
                                                <span>1800-4000</span>
                                            </li>
                                            <li class="price__item">
                                                <span>Жакет женский без подкладки</span>
                                                <span>4000-7000</span>
                                            </li>
                                            <li class="price__item">
                                                <span>Жакет женский на подкладке</span>
                                                <span>6000-8000</span>
                                            </li>
                                            <li class="price__item">
                                                <span>Жакет (сложное моделирование)</span>
                                                <span>6000-10000</span>
                                            </li>
                                            <li class="price__item">
                                                <span>Пиджак мужской</span>
                                                <span>8000-15000</span>
                                            </li>
                                            <li class="price__item">
                                                <span>Жилет (ж/м)</span>
                                                <span>1800-5000</span>
                                            </li>
                                            <li class="price__item">
                                                <span>Платье</span>
                                                <span>2000-5000</span>
                                            </li>
                                            <li class="price__item">
                                                <span>Платье (сложное моделирование)</span>
                                                <span>4000-8000</span>
                                            </li>
                                            <li class="price__item">
                                                <span>Платье свадебное, вечернее</span>
                                                <span>5000-20000</span>
                                            </li>
                                            <li class="price__item">
                                                <span>Блуза, рубашка мужская</span>
                                                <span>2000-5000</span>
                                            </li>
                                            <li class="price__item">
                                                <span>Пальто (ж/м)</span>
                                                <span>5000-12000</span>
                                            </li>
                                            <li class="price__item">
                                                <span>Куртка, плащ</span>
                                                <span>5000-10000</span>
                                            </li>
                                        </ul>
                                        <ul class="price__list">
                                            <li class="price__item">
                                                <span>Изделия из натуральной кожи меха</span>
                                                <span>+100%</span>
                                            </li>
                                            <li class="price__item">
                                                <span>Ускоренный срок исполнения</span>
                                                <span>+50-100%</span>
                                            </li>
                                            <li class="price__item">
                                                <span>Изделия из сложных в обработке материалов</span>
                                                <span>+20-50%</span>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li class="accordion__item">
                            <h2 class="title">
                                Пошив штор и домашнего текстиля
                            </h2>
                            <div class="content">
                                <div class="mdc-layout-grid__inner">
                                    <div
                                        class="mdc-layout-grid__cell mdc-layout-grid__cell--span-1-tablet mdc-layout-grid__cell--span-2-desktop d-n d-b-t">
                                    </div>
                                    <div
                                        class="mdc-layout-grid__cell mdc-layout-grid__cell--span-7-tablet mdc-layout-grid__cell--span-10-desktop">
                                        <ul class="price__list">
                                            <li class="price__item">
                                                <span>Штора одинарная (до 1.6м шириной)</span>
                                                <span>200</span>
                                            </li>
                                            <li class="price__item">
                                                <span>Штора двойная (до 3м шириной)</span>
                                                <span>250</span>
                                            </li>
                                            <li class="price__item">
                                                <span>Тюль</span>
                                                <span>80-140 грн/м</span>
                                            </li>
                                            <li class="price__item">
                                                <span>Наволочки (в том числе декоративные)</span>
                                                <span>100-400</span>
                                            </li>
                                            <li class="price__item">
                                                <span>Простынь</span>
                                                <span>150-200</span>
                                            </li>
                                            <li class="price__item">
                                                <span>Пододеяльник</span>
                                                <span>180-350</span>
                                            </li>
                                            <li class="price__item">
                                                <span>Чехлы</span>
                                                <span>от 160</span>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li class="accordion__item">
                            <h2 class="title">
                                Ремонт одежды
                            </h2>
                            <div class="content">
                                <div class="mdc-layout-grid__inner">
                                    <div
                                        class="mdc-layout-grid__cell mdc-layout-grid__cell--span-1-tablet mdc-layout-grid__cell--span-2-desktop d-n d-b-t">
                                    </div>
                                    <div
                                        class="mdc-layout-grid__cell mdc-layout-grid__cell--span-7-tablet mdc-layout-grid__cell--span-10-desktop">
                                        <ul class="accordion js-hook__accordion">
                                            <li class="accordion__item">
                                                <strong class="title">
                                                    Укоротить:
                                                </strong>
                                                <div class="content">
                                                    <ul class="price__list">
                                                        <li class="price__item">
                                                            <span>брюки/ юбка/ блуза/ рубашка/ футболка</span>
                                                            <span>160-350</span>
                                                        </li>
                                                        <li class="price__item">
                                                            <span>платье свадебное/ вечернее</span>
                                                            <span>400-1500</span>
                                                        </li>
                                                        <li class="price__item">
                                                            <span>низ верхней одежды</span>
                                                            <span>250-600</span>
                                                        </li>
                                                        <li class="price__item">
                                                            <span>натуральная шуба/дублёнка/куртка, плащ из натуральной
                                                                кожи</span>
                                                            <span>450-1500</span>
                                                        </li>
                                                        <li class="price__item">
                                                            <span>низ рукавов платья/блузы/рубашки/гольфа</span>
                                                            <span>160-250</span>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </li>
                                            <li class="accordion__item">
                                                <strong class="title">
                                                    Заменить молнию:
                                                </strong>
                                                <div class="content">
                                                    <ul class="price__list">
                                                        <li class="price__item">
                                                            <span>юбка/брюки/блуза/платье</span>
                                                            <span>160-300</span>
                                                        </li>
                                                        <li class="price__item">
                                                            <span>верхняя одежда</span>
                                                            <span>250-400</span>
                                                        </li>
                                                        <li class="price__item">
                                                            <span>изделия из натуральной кожи/меха</span>
                                                            <span>400-1000</span>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </li>
                                            <li class="accordion__item">
                                                <strong class="title">
                                                    Ушить/расшить по объёму:
                                                </strong>
                                                <div class="content">
                                                    <ul class="price__list">
                                                        <li class="price__item">
                                                            <span>юбка/брюки/блуза/платье</span>
                                                            <span>160-700</span>
                                                        </li>
                                                        <li class="price__item">
                                                            <span>верхняя одежда</span>
                                                            <span>250-1200</span>
                                                        </li>
                                                        <li class="price__item">
                                                            <span>изделия из натуральной кожи/меха</span>
                                                            <span>1000-2000</span>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </li>
                                            <li class="accordion__item">
                                                <strong class="title">
                                                    Мелкий ремонт и реставрация:
                                                </strong>
                                                <div class="content">
                                                    <ul class="price__list">
                                                        <li class="price__item">
                                                            <span>простая строчка, оверлок</span>
                                                            <span>7-10 грн/м</span>
                                                        </li>
                                                        <li class="price__item">
                                                            <span>сложные виды швов («московский», пикко, запошивочный,
                                                                «в замок» и
                                                                т.д.</span>
                                                            <span>10-40 грн/м</span>
                                                        </li>
                                                        <li class="price__item">
                                                            <span>установка металлической фурнитуры (заклёпки, кнопки,
                                                                люверсы,
                                                                крючки)</span>
                                                            <span>15-40 грн/шт</span>
                                                        </li>
                                                        <li class="price__item">
                                                            <span>мелкий ремонт одежды</span>
                                                            <span>от 20</span>
                                                        </li>
                                                        <li class="price__item">
                                                            <span>изготовить петлю</span>
                                                            <span>20-80</span>
                                                        </li>
                                                        <li class="price__item">
                                                            <span>пришить пуговицу</span>
                                                            <span>5-30</span>
                                                        </li>
                                                        <li class="price__item">
                                                            <span>штопка, латка</span>
                                                            <span>50-300</span>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li class="accordion__item">
                            <h2 class="title">
                                Услуги личного портного
                            </h2>
                            <div class="content">
                                <div class="mdc-layout-grid__inner">
                                    <div
                                        class="mdc-layout-grid__cell mdc-layout-grid__cell--span-1-tablet mdc-layout-grid__cell--span-2-desktop d-n d-b-t">
                                    </div>
                                    <div
                                        class="mdc-layout-grid__cell mdc-layout-grid__cell--span-7-tablet mdc-layout-grid__cell--span-10-desktop">
                                        <ul class="accordion js-hook__accordion">
                                            <li class="accordion__item">
                                                <strong class="title">
                                                    <sup>*</sup>Выезд на дом:
                                                </strong>
                                                <div class="content">
                                                    <ul class="price__list">
                                                        <li class="price__item">
                                                            <span>по району</span>
                                                            <span>150</span>
                                                        </li>
                                                        <li class="price__item">
                                                            <span>другие районы Киева</span>
                                                            <span>200</span>
                                                        </li>
                                                        <li class="price__item">
                                                            <span>за город</span>
                                                            <span>250 + транспортные расходы</span>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </li>
                                        </ul>
                                        <ul class="price__list">
                                            <li class="price__item">
                                                <span><sup>*</sup>Услуги доставки (при необходимости) оплачиваются по
                                                    тарифу услуг такси</span>
                                                <span></span>
                                            </li>
                                            <li class="price__item">
                                                <span><sup>*</sup>Поход по магазинам, выбор ткани, фурнитуры</span>
                                                <span>100 грн/ч</span>
                                            </li>
                                            <li class="price__item">
                                                <span><sup>*</sup>Прорисовка эскизов по договорённости (зависит от
                                                    сложности и объёмов задачи)</span>
                                                <span></span>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li class="accordion__item">
                            <h2 class="title">
                                Химчистка
                            </h2>
                            <div class="content">
                                <div class="mdc-layout-grid__inner">
                                    <div
                                        class="mdc-layout-grid__cell mdc-layout-grid__cell--span-1-tablet mdc-layout-grid__cell--span-2-desktop d-n d-b-t">
                                    </div>
                                    <div
                                        class="mdc-layout-grid__cell mdc-layout-grid__cell--span-7-tablet mdc-layout-grid__cell--span-10-desktop">
                                        <ul class="accordion js-hook__accordion">
                                            <li class="accordion__item">
                                                <strong class="title">
                                                    Химчистка и современная aqua-чистка текстиля
                                                </strong>
                                                <div class="content">
                                                    <ul class="price__list">
                                                        <li class="price__item">
                                                            <span>Костюм(двойка/тройка)</span>
                                                            <span>400/450</span>
                                                        </li>
                                                        <li class="price__item">
                                                            <span>Рубашка, блуза</span>
                                                            <span>150-20</span>
                                                        </li>
                                                        <li class="price__item">
                                                            <span>Брюки, юбка</span>
                                                            <span>180</span>
                                                        </li>
                                                        <li class="price__item">
                                                            <span>Шарф, платок, галстук, шапка</span>
                                                            <span>150</span>
                                                        </li>
                                                        <li class="price__item">
                                                            <span>Свитер</span>
                                                            <span> 220-450</span>
                                                        </li>
                                                        <li class="price__item">
                                                            <span>Платье</span>
                                                            <span>350</span>
                                                        </li>
                                                        <li class="price__item">
                                                            <span>Платье шёлковое вечернее</span>
                                                            <span>450</span>
                                                        </li>
                                                        <li class="price__item">
                                                            <span>Платье свадебное</span>
                                                            <span>800-1000</span>
                                                        </li>
                                                        <li class="price__item">
                                                            <span>Куртка утеплённая</span>
                                                            <span>300-400</span>
                                                        </li>
                                                        <li class="price__item">
                                                            <span>Куртка на пуху</span>
                                                            <span>400-500</span>
                                                        </li>
                                                        <li class="price__item">
                                                            <span>Плащ</span>
                                                            <span>300-400</span>
                                                        </li>
                                                        <li class="price__item">
                                                            <span>Пальто демисезонное</span>
                                                            <span>350-450</span>
                                                        </li>
                                                        <li class="price__item">
                                                            <span>Дублёнка, искусственная шуба</span>
                                                            <span>500-600</span>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </li>
                                            <li class="accordion__item">
                                                <strong class="title">
                                                    Чистка + покраска (восстановление родного цвета) изделий из кожи и
                                                    замши:
                                                </strong>
                                                <div class="content">
                                                    <ul class="price__list">
                                                        <li class="price__item">
                                                            <span>Куртка</span>
                                                            <span>800-1000</span>
                                                        </li>
                                                        <li class="price__item">
                                                            <span>Пальто</span>
                                                            <span>950-1200</span>
                                                        </li>
                                                        <li class="price__item">
                                                            <span>Дублёнка</span>
                                                            <span>900-1100</span>
                                                        </li>
                                                        <li class="price__item">
                                                            <span>Юбка, шорты, брюки</span>
                                                            <span>600-800</span>
                                                        </li>
                                                        <li class="price__item">
                                                            <span>Перчатки</span>
                                                            <span>450</span>
                                                        </li>
                                                        <li class="price__item">
                                                            <span>Ремень</span>
                                                            <span>400</span>
                                                        </li>
                                                        <li class="price__item">
                                                            <span>Клатч, кошелёк</span>
                                                            <span>500</span>
                                                        </li>
                                                        <li class="price__item">
                                                            <span>Сумка, рюкзак (от размера)</span>
                                                            <span>700-900</span>
                                                        </li>
                                                        <li class="price__item">
                                                            <span>Босоножки, туфли, кроссовки</span>
                                                            <span>500-700</span>
                                                        </li>
                                                        <li class="price__item">
                                                            <span>Ботинки, сапоги, ботфорты</span>
                                                            <span>800-1000</span>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </li>
                                            <li class="accordion__item">
                                                <strong class="title">
                                                    Чистка изделий из натурального меха:
                                                </strong>
                                                <div class="content">
                                                    <ul class="price__list">
                                                        <li class="price__item">
                                                            <span>Шуба</span>
                                                            <span>1200-1500</span>
                                                        </li>
                                                        <li class="price__item">
                                                            <span>Воротник, манжеты, шапка</span>
                                                            <span>500</span>
                                                        </li>
                                                        <li class="price__item">
                                                            <span>Жилет, подстёжка</span>
                                                            <span>700-1000</span>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
                <div
                    class="mdc-layout-grid__cell mdc-layout-grid__cell--span-1-tablet mdc-layout-grid__cell--span-3-desktop d-n d-b-t">
                </div>
            </div>
            <div class="mdc-layout-grid__inner price__inner">
                <div
                    class="mdc-layout-grid__cell mdc-layout-grid__cell--span-1-phone mdc-layout-grid__cell--span-3-tablet mdc-layout-grid__cell--span-7-desktop">
                </div>
                <div
                    class="mdc-layout-grid__cell mdc-layout-grid__cell--span-3-phone mdc-layout-grid__cell--span-5-tablet mdc-layout-grid__cell--span-5-desktop">
                    <p class="price__desc">(Цены указаны в гривнах)</p>
                    <p class="price__desc">*Данный прайс является ориентиром. Он очерчивает границы стоимости наших
                        услуг с учетом ваших
                        пожеланий и наших возможностей. Диалог, дает возможность озвучить вам окончательную
                        стоимость поставленной перед нами задачи. Мы вас внимательно слушаем…</p>
                </div>
            </div>
        </div>
    </section>
</main>


<?php
get_footer();