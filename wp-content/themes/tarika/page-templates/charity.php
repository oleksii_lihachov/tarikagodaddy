<?php
/*
Template Name: Charity
*/

get_header();
?>
	<main id="primary" class="site-main">
		<section class="charity section-indent">
			<div class="mdc-layout-grid">

				<div class="mdc-layout-grid__inner">
					<div class="mdc-layout-grid__cell mdc-layout-grid__cell--span-1-tablet mdc-layout-grid__cell--span-2-desktop d-n d-b-t"></div>
					<div class="mdc-layout-grid__cell mdc-layout-grid__cell--span-7-tablet mdc-layout-grid__cell--span-10-desktop">
						<h1><?php echo get_the_title(); ?></h1>
					</div>
				</div>

				<div class="mdc-layout-grid__inner">
					<div class="mdc-layout-grid__cell mdc-layout-grid__cell--span-2-tablet mdc-layout-grid__cell--span-3-desktop d-n d-b-t"></div>
					<div class="mdc-layout-grid__cell mdc-layout-grid__cell--span-6-tablet mdc-layout-grid__cell--span-8-desktop">
						<h3 class="charity__subtitle">Тему благих дел, мы не станем освещать долго и подробно.<br
									class="d-n d-i-d"/>В этом разделе стоит задача
							рассказать, как это можно делать в нашем ателье.<br class="d-n d-i-d"/> Сегодня можем
							предложить два варианта:</h3>
					</div>
					<div class="mdc-layout-grid__cell mdc-layout-grid__cell--span-1-desktop d-n d-b-d"></div>
				</div>

				<div class="mdc-layout-grid__inner">
					<div class="mdc-layout-grid__cell mdc-layout-grid__cell--span-4-tablet mdc-layout-grid__cell--span-6-desktop">

						<div class="mdc-layout-grid__inner">
							<div class="mdc-layout-grid__cell mdc-layout-grid__cell--span-1-tablet mdc-layout-grid__cell--span-2-desktop d-n d-b-t">
								<div class="text-right">
									<span class="h1">1</span>
								</div>
							</div>
							<div class="mdc-layout-grid__cell mdc-layout-grid__cell--span-4-phone mdc-layout-grid__cell--span-7-tablet mdc-layout-grid__cell--span-10-desktop">
								<div class="charity__inner-copy">
									<span class="h1 d-n-t">1</span>
									<p>Знаете ли вы, что такое «подвешенный кофе»?<br/>Люди, оплачивая два кофе, выпивают
										один,
										второй же остается тому, кому он необходим в определенный момент. В ателье «TaRika »
										есть
										фонд «подвешенных» фурнитуры/остатков ткани, а также финансовый фонд для людей,
										сдающих в
										ремонт одежду (любой может попасть в ситуацию, когда будет нуждаться в скидке).
										Каждый из
										вас может как «подвесить» эту скидку/фурнитуру/остатки, так и воспользоваться
										«подвешенными»
										кем-то ранее.<br/>Почему мы решили об этом написать? Уже не раз некоторые из вас
										оставляли
										лежащие без дела дома молнии, пуговицы, нитки (совсем недавно под дверью ателье был
										обнаружен пакет с тканями — Дед Мороз существует?) А в разговорах с другими
										выяснялось, что
										они не приносили, поскольку не подозревали «...что вообще можно и это так
										просто...». А коль
										есть спрос, мы решили сделать такую себе «швейную территорию добрых дел» в нашем
										ателье
										«TaRika».
									</p>
								</div>
							</div>
						</div>
					</div>

					<div class="mdc-layout-grid__cell mdc-layout-grid__cell--span-4-tablet mdc-layout-grid__cell--span-6-desktop">
						<div class="charity__image">
							<?php echo get_the_post_thumbnail(); ?>
						</div>

						<div class="mdc-layout-grid__inner">
							<div class="mdc-layout-grid__cell mdc-layout-grid__cell--span-1-tablet mdc-layout-grid__cell--span-2-desktop d-n d-b-t">
								<div class="text-right">
									<span class="h1">2</span>
								</div>
							</div>
							<div class="mdc-layout-grid__cell mdc-layout-grid__cell--span-7-tablet mdc-layout-grid__cell--span-10-desktop">
								<div class="charity__inner-copy">
									<span class="h1 d-n-t">2</span>
									<div>
										<p>Вы можете принести чистые и аккуратно сложенные вещи и обувь для детей, что живут в детских домах. Принимая их у вас, мы расскажем, куда именно они уедут.</p>
										<p>При желании, после того, как вещи переданы детям, вы получите на Вайбер отчетные фото с вашими подарками (наши подопечные с удовольствием позируют в обновках). Оговорка: принимаем только постиранные и наутюженные вещи, чистую и хорошую обувь.</p>
									</div>
								</div>
							</div>
						</div>

					</div>
				</div>

				<div class="mdc-layout-grid__inner">
					<div class="mdc-layout-grid__cell mdc-layout-grid__cell--span-8-tablet mdc-layout-grid__cell--span-11-desktop">
						<div class="mini-contacts">
							<p>Вещи мы примем по адресу:<br><strong>г. Киев, просп. Петра Григоренка, 38А, <span
											class="no-wrap">офис
										86</span></strong></p>
							<a href="/contacts" class="more no-wrap">Посмотреть карту</a>
						</div>
					</div>
					<div class="mdc-layout-grid__cell mdc-layout-grid__cell--span-1-desktop d-n d-b-d"></div>
				</div>

				<div class="mdc-layout-grid__inner">
					<div class="mdc-layout-grid__cell mdc-layout-grid__cell--span-8-tablet mdc-layout-grid__cell--span-12-desktop">
						<footer class="charity__footer">
							<h3>Все просто: мы — проводники тепла ваших сердец.<br class="d-n d-i-t"/>Когда у вас его избыток, мы готовы
								лично<br class="d-n d-i-t d-n-d"/> передать тем, у кого недостаток.<br/>Делать добро вкусно. Угощайтесь)</h3>
						</footer>
					</div>
				</div>
			</div>
		</section>
	</main>
<?php

get_footer();
