<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package tarika
 */

$fb_link    = get_option( 'facebook_link' );
$insta_link = get_option( 'instagram_link' );
$work_phone = get_option( 'work_phone' );
$work_email = get_option( 'work_email' );
?>

<footer id="colophon" class="footer">
	<div class="mdc-layout-grid">
		<div class="mdc-layout-grid__inner">

			<div class="mdc-layout-grid__cell mdc-layout-grid__cell--span-3-phone mdc-layout-grid__cell--span-2-tablet mdc-layout-grid__cell--span-2-desktop">
				<?php echo do_shortcode( '[menu name=”footer-main” class="footer-links"]' ); ?>
			</div>

			<div class="footer__back-cell mdc-layout-grid__cell mdc-layout-grid__cell--span-1-phone mdc-layout-grid__cell--span-2-tablet mdc-layout-grid__cell--span-2-desktop">
				<a href="#" class="back-to-top js-hook__back-to-top">
					<svg width="10" height="12" viewBox="0 0 10 12" fill="none" xmlns="http://www.w3.org/2000/svg">
						<path d="M1 1L8.14286 5.99999L1 11" stroke="#101F32" stroke-width="2" stroke-linecap="round"
							  stroke-linejoin="round"/>
					</svg>
				</a>
			</div>

			<div class="footer__social-cell mdc-layout-grid__cell mdc-layout-grid__cell--span-4-tablet mdc-layout-grid__cell--span-8-desktop">
				<div class="socials">
					<?php if ( ! empty( $fb_link ) ): ?>
						<a href="<?php echo esc_url( $fb_link ); ?>"
						   class="socials__item"
						   target="_blank">
							<img src="<?php echo get_template_directory_uri() . '/assets/images/socials/facebook-white.svg' ?>"
								 alt="Facebook icon"/>
						</a>
					<?php endif; ?>
					<?php if ( ! empty( $insta_link ) ): ?>
						<a href="<?php echo esc_url( $insta_link ); ?>"
						   class="socials__item"
						   target="_blank">
							<img src="<?php echo get_template_directory_uri() . '/assets/images/socials/instagram-white.svg' ?>"
								 alt="Instagram icon"/>
						</a>
					<?php endif; ?>
					<?php if ( ! empty( $work_phone ) ): ?>
						<a href="tel:<?php echo esc_attr( $work_phone ); ?>"
						   class="socials__item">
							<img src="<?php echo get_template_directory_uri() . '/assets/images/socials/viber-white.svg' ?>"
								 alt="Viber icon"/>
						</a>
					<?php endif; ?>
					<?php if ( ! empty( $work_email ) ): ?>
						<a href="mailto:<?php echo esc_attr( get_option( 'work_email' ) ); ?>"
						   class="socials__item">
							<img src="<?php echo get_template_directory_uri() . '/assets/images/socials/email-white.svg' ?>"
								 alt="Email icon"/>
						</a>
					<?php endif; ?>
				</div>
			</div>

			<?php if ( get_option( 'include_footer_links_settings' ) ): ?>
				<div class="footer__links-settings-cell mdc-layout-grid__cell mdc-layout-grid__cell--span-6-tablet mdc-layout-grid__cell--span-8-desktop">
					<?php echo do_shortcode( '[menu name=”footer-settings” class="footer-links-settings"]' ); ?>
				</div>
			<?php endif; ?>

			<div class="footer__copyright-cell mdc-layout-grid__cell mdc-layout-grid__cell--span-2-tablet mdc-layout-grid__cell--span-2-desktop">
				<p class="copyright">© TaRika 2021 </p>
			</div>
		</div>
	</div>
</footer>
</div>

<?php wp_footer(); ?>

</body>
</html>
