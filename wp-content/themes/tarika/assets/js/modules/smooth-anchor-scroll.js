import {enableBodyScroll} from 'body-scroll-lock';

export default jQuery(document).ready(function ($) {
	const $links = $('.js--anchor-link a');
	const $header = $('.header');
	const $hamburger = $('.hamburger');
	const $nav = $('.navigation');


	if ($links.length) {
		$links.on('click', function (e) {
			e.stopPropagation();
			e.preventDefault();
			const id = $(this).attr('href');
			const scrollOffset = 50;

			$('html, body').animate({
				scrollTop: $(id).offset().top - scrollOffset
			}, 400);

			if ($hamburger.length) {
				enableBodyScroll($nav.get(0));
				$header.removeClass('open');
				$hamburger.removeClass('active');
				$nav.removeClass('open');
			}
		});
	}
});