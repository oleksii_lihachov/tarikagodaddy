export default jQuery(document).ready(function ($) {
	const $workSlider = $('.js-hook__works-slider');
	const $shopSlider = $('.js-hook_shop-slider');

	if ($workSlider.length) {
		$workSlider.slick({
			centerMode: true,
			slidesToShow: 1,
			arrows: false,
			variableWidth: true
		});
	}

	if ($shopSlider.length) {
		$shopSlider.slick({
			// centerMode: true,
			slidesToShow: 1,
			arrows: false,
			variableWidth: true
		});
	}
});