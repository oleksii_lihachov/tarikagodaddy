export default jQuery(document).ready(function ($) {
	const $trigger = $('.js-hook__more');
	const $triggerContent = $('div[data-id]');

	if ($trigger.length && $triggerContent.length) {
		$trigger.on('click', function () {
			const targetId = $(this).data('target');
			const $element = $(`[data-id="${targetId}"]`);

			if ($element) {
				$(this).hide();
				$element.slideDown();
			}
		});

		$triggerContent.on('click', function () {
			const id = $(this).data('id');

			$(`div[data-id="${id}"]`).slideUp();
			$(`.js-hook__more[data-target="${id}"]`).show();
		});
	}
});