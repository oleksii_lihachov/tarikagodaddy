export default jQuery(document).ready(function ($) {
	const $accordions = $('.js-hook__accordion');
	if ($accordions.length) {
		$accordions.each(function (index, accordion) {
			const $title = $(accordion).find('> li > .title');

			$title.on('click', function (ev) {
				const dropDown = $(this).closest('li').find('> .content');

				$(this).closest('.js-hook__accordion').find('.content').not(dropDown).slideUp();

				if ($(this).hasClass('active')) {
					$(this).removeClass('active');
				} else {
					$(this).closest('.js-hook__accordion').find('.title.active').removeClass('active');
					$(this).addClass('active');
				}
				dropDown.stop(false, true).slideToggle();
				ev.preventDefault();
			});
		});
	}
});