<?php

class Options_Page {
	public function __construct() {
		add_action( 'admin_menu', array( $this, 'admin_menu' ) );
		add_action( 'admin_init', array( $this, 'register_settings' ) );
	}

	public function admin_menu() {
		add_options_page(
			'Опции сайта',
			'Опции сайта',
			'manage_options',
			'site-options',
			array( $this, 'settings_page' )
		);
	}

	public function register_settings() {
		//	Google Api
		add_settings_section(
			'google_keys_section', // section ID
			__( 'Google Ключи', 'tarika' ), // title (if needed)
			'', // callback function (if needed)
			'site-options' // page slug
		);

		add_settings_field(
			'google_api_key',
			'Api key',
			array( $this, 'api_key_html' ), // function which prints the field
			'site-options', // page slug
			'google_keys_section', // section ID
			array(
				'label_for' => 'google_api_key',
			)
		);

		register_setting(
			'site-options', // settings group name
			'google_api_key', // option name
			'sanitize_text_field' // sanitization function
		);


		// Social Links
		add_settings_section(
			'social_links_section', // section ID
			__( 'Социальные сети', 'tarika' ), // title (if needed)
			'', // callback function (if needed)
			'site-options' // page slug
		);

		add_settings_field(
			'facebook_link',
			'Facebook',
			array( $this, 'facebook_link_html' ), // function which prints the field
			'site-options', // page slug
			'social_links_section', // section ID
			array(
				'label_for' => 'facebook_link',
			)
		);

		add_settings_field(
			'instagram_link',
			'Instagram',
			array( $this, 'instagram_link_html' ), // function which prints the field
			'site-options', // page slug
			'social_links_section', // section ID
			array(
				'label_for' => 'instagram_link',
			)
		);

		add_settings_field(
			'work_phone',
			__( 'Tел. химчистка, ателье:', 'tarika' ),
			array( $this, 'work_phone_html' ), // function which prints the field
			'site-options', // page slug
			'social_links_section', // section ID
			array(
				'label_for' => 'work_phone',
			)
		);

		add_settings_field(
			'studio_phone',
			__( 'Tел. магазин,  обучение,  благотворительность:', 'tarika' ),
			array( $this, 'studio_phone_html' ), // function which prints the field
			'site-options', // page slug
			'social_links_section', // section ID
			array(
				'label_for' => 'studio_phone',
			)
		);

		add_settings_field(
			'work_email',
			__( 'Рабочая почта', 'tarika' ),
			array( $this, 'work_email_html' ), // function which prints the field
			'site-options', // page slug
			'social_links_section', // section ID
			array(
				'label_for' => 'work_email',
			)
		);

		register_setting(
			'site-options', // settings group name
			'facebook_link', // option name
			'sanitize_text_field' // sanitization function
		);

		register_setting(
			'site-options', // settings group name
			'instagram_link', // option name
			'sanitize_text_field' // sanitization function
		);

		register_setting(
			'site-options', // settings group name
			'work_phone', // option name
			'sanitize_text_field' // sanitization function
		);

		register_setting(
			'site-options',
			'studio_phone',
			'sanitize_text_field'
		);

		register_setting(
			'site-options', // settings group name
			'work_email', // option name
			'sanitize_text_field' // sanitization function
		);

		// UI render
		add_settings_section(
			'ui_render_section', // section ID
			__( 'Включить секции на сайте', 'tarika' ), // title (if needed)
			'', // callback function (if needed)
			'site-options' // page slug
		);

		add_settings_field(
			'include_footer_links_settings',
			__( 'Cлужебное меню в футере', 'tarika' ),
			array( $this, 'include_footer_links_settings_html' ), // function which prints the field
			'site-options', // page slug
			'ui_render_section', // section ID
			array(
				'label_for' => 'include_footer_links_settings',
			)
		);

		add_settings_field(
			'include_our_works',
			__( 'Наши работы', 'tarika' ),
			array( $this, 'include_our_works_html' ), // function which prints the field
			'site-options', // page slug
			'ui_render_section', // section ID
			array(
				'label_for' => 'include_our_works',
			)
		);

		add_settings_field(
			'include_shop',
			__( 'Магазин', 'tarika' ),
			array( $this, 'include_shop_html' ), // function which prints the field
			'site-options', // page slug
			'ui_render_section', // section ID
			array(
				'label_for' => 'include_shop',
			)
		);

		add_settings_field(
			'include_portfolio',
			__( 'Портфолио', 'tarika' ),
			array( $this, 'include_portfolio_html' ), // function which prints the field
			'site-options', // page slug
			'ui_render_section', // section ID
			array(
				'label_for' => 'include_portfolio',
			)
		);

		register_setting(
			'site-options', // settings group name
			'include_footer_links_settings', // option name
			'sanitize_text_field' // sanitization function
		);

		register_setting(
			'site-options', // settings group name
			'include_our_works', // option name
			'sanitize_text_field' // sanitization function
		);

		register_setting(
			'site-options', // settings group name
			'include_shop', // option name
			'sanitize_text_field' // sanitization function
		);

		register_setting(
			'site-options', // settings group name
			'include_portfolio', // option name
			'sanitize_text_field' // sanitization function
		);
	}

	public function api_key_html() {
		$api_key = get_option( 'google_api_key' );

		printf(
			'<input type="text" id="google_api_key" name="google_api_key" value="%s" />',
			esc_attr( $api_key )
		);
	}

	public function facebook_link_html() {
		$link = get_option( 'facebook_link' );

		printf(
			'<input type="url" id="facebook_link" name="facebook_link" value="%s" />',
			esc_url( $link )
		);
	}

	public function instagram_link_html() {
		$link = get_option( 'instagram_link' );

		printf(
			'<input type="url" id="instagram_link" name="instagram_link" value="%s" />',
			esc_url( $link )
		);
	}

	public function work_phone_html() {
		$phone = get_option( 'work_phone' );

		printf(
			'<input type="tel" id="work_phone" name="work_phone" value="%s" />',
			esc_attr( $phone )
		);
	}

	public function studio_phone_html() {
		$phone = get_option( 'studio_phone' );

		printf(
			'<input type="tel" id="studio_phone" name="studio_phone" value="%s" />',
			esc_attr( $phone )
		);
	}

	public function work_email_html() {
		$email = get_option( 'work_email' );

		printf(
			'<input type="email" id="work_email" name="work_email" value="%s" />',
			esc_attr( $email )
		);
	}

	public function include_footer_links_settings_html() {
		$enable  = get_option( 'include_footer_links_settings' );
		$checked = $enable ? 'checked' : '';

		echo '<input type="checkbox" id="include_footer_links_settings" name="include_footer_links_settings" ' . $checked . '/>';
	}

	public function include_our_works_html() {
		$enable  = get_option( 'include_our_works' );
		$checked = $enable ? 'checked' : '';

		echo '<input type="checkbox" id="include_our_works" name="include_our_works" ' . $checked . '/>';
	}

	public function include_shop_html() {
		$enable  = get_option( 'include_shop' );
		$checked = $enable ? 'checked' : '';

		echo '<input type="checkbox" id="include_shop" name="include_shop" ' . $checked . '/>';
	}

	public function include_portfolio_html() {
		$enable  = get_option( 'include_portfolio' );
		$checked = $enable ? 'checked' : '';

		echo '<input type="checkbox" id="include_portfolio" name="include_portfolio" ' . $checked . '/>';
	}

	public function settings_page() {
		?>
		<div class="wrap">
			<h1>Site Options</h1>
			<hr/>
			<form method="post" action="options.php">
				<?php
				settings_fields( 'site-options' );
				do_settings_sections( 'site-options' );
				submit_button();
				?>
			</form>
		</div>
		<?php
	}
}
